<?php

declare(strict_types=1);

use App\Strategy\Message;
use App\Strategy\MessageContext;
use PHPUnit\Framework\MockObject\MockObject;

class MessageStrategyTest extends PHPUnit\Framework\TestCase
{
    protected MessageContext $messageContext;

    protected Message|MockObject $message;

    public function setUp(): void
    {
        $this->message = $this->createMock(Message::class);
        $this->messageContext = new MessageContext(
            $this->message
        );
    }

    public function testGetMsgDayOfWeekWhenMessageIsTheSame(): void
    {
        $this->message->expects($this->once())
            ->method('getMessage')
            ->willReturn('Hoje é segunda-feira.');

        $message = $this->messageContext->getMsgDayOfWeek();
        $this->assertEquals('Hoje é segunda-feira.', $message);
    }

    public function testGetMsgDayOfWeekWhenMessageIsNotNull(): void
    {
        $this->message->expects($this->once())
            ->method('getMessage')
            ->willReturn('Hoje é segunda-feira.');

        $message = $this->messageContext->getMsgDayOfWeek();
        $this->assertNotNull($message);
    }

    public function testGetMsgDayOfWeekWhenMessageIsString(): void
    {
        $this->message->expects($this->once())
            ->method('getMessage')
            ->willReturn('Hoje é quarta-feira.');

        $message = $this->messageContext->getMsgDayOfWeek();
        $this->assertIsString($message);
    }

    public function testGetMsgDayOfWeekWhenMessageIsEmpty(): void
    {
        $this->message->expects($this->once())
            ->method('getMessage')
            ->willReturn('');

        $message = $this->messageContext->getMsgDayOfWeek();
        $this->assertEmpty($message);
    }

    public function testGetMsgDayOfWeekWhenMessageIsNotEmpty(): void
    {
        $this->message->expects($this->once())
            ->method('getMessage')
            ->willReturn('Hoje é quarta-feira.');

        $message = $this->messageContext->getMsgDayOfWeek();
        $this->assertNotEmpty($message);
    }

    public function testGetMsgDayOfWeekWhenMessageException(): void
    {
        $this->message->expects($this->once())
            ->method('getMessage')
            ->willThrowException(new \Exception('Dia da semana inválido.'));

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Dia da semana inválido.');

        $this->messageContext->getMsgDayOfWeek();
    }
}