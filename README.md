# Desafio - Programador PHP

## Descrição

Desenvolva um programa que exiba uma mensagem diferente para cada dia da
semana, usando o padrão Strategy. Pense que em datas especiais, podemos ter alguma
variação.

## Pré-Requisitos

- git com link público;
- testes unitário;
- php;

## Instalação

Siga o passo a passo sobre como instalar e configurar do projeto:


#### 1. Clone o repositório:
```sh
git clone https://gitlab.com/djonathanassis/dayofweek-strategy.git
```

#### 2. Navegue até a pasta do projeto:
```sh
cd DesignPatternsPHP
```

#### 3. Iniciar os Contêineres Docker:
```sh
docker-compose up -d
```

#### 4. Instale as dependências usando o Composer (ou outro gerenciador de pacotes):
```sh
docker-compose exec app composer install
```

Acesse a aplicação em seu navegador: **[http://localhost:8888](http://localhost:8888)**

## Teste Unitario

Siga os passo a passo para rodar teste unitários:

#### 3. Comando para rodar os testes unitários:

```sh
 ./vendor/bin/phpunit
```