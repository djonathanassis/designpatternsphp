<?php
declare(strict_types=1);

use App\Observer\EmailNotifier;
use App\Observer\Medias\Desktop;
use App\Observer\Medias\Phone;

date_default_timezone_set('America/Sao_Paulo');
require_once __DIR__ . '/../vendor/autoload.php';

use App\Strategy\DayOfWeek\Days;
use App\Strategy\Message;
use App\Strategy\MessageContext;

$message = new MessageContext(
    new Message(
        new Days('2021-09-01')
    )
);
$response = $message->getMsgDayOfWeek();

echo $response;



//$emailNotifier = new EmailNotifier();
//
//$desktop = new Desktop('MeuAppDesktop');
//$mobile = new Phone('MeuAppMobile');
//
//$emailNotifier->attachNotify($desktop);
//$emailNotifier->attachNotify($mobile);
//
//$emailNotifier->newNotify("Olá, você tem um novo e-mail!");
//
//$emailNotifier->detachNotify($desktop);
//
//$emailNotifier->newNotify("Outro e-mail chegou!");
