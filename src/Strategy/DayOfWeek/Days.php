<?php

declare(strict_types=1);

namespace App\Strategy\DayOfWeek;

use App\Strategy\Interfaces\MessageInterface;

class Days implements MessageInterface
{
    public function __construct(
        protected ?string $dateTime = null
    )
    {
    }

    /**
     * @throws \Exception
     */
    public function getMessage(): string
    {
        return $this->selectMessageOfDay();
    }

    /**
     * @throws \Exception
     */
    private function selectMessageOfDay(): string
    {
        $day = (int)(new \DateTime($this->dateTime))->format('N');

        return match ($day) {
            1 => 'Hoje é segunda-feira.',
            2 => 'Hoje é terça-feira.',
            3 => 'Hoje é quarta-feira.',
            4 => 'Hoje é quinta-feira.',
            5 => 'Hoje é sexta-feira.',
            6 => 'Hoje é sábado.',
            7 => 'Hoje é domingo.',
            default => throw new \Exception('Dia da semana inválido.')
        };
    }
}