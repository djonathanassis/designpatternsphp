<?php

namespace App\Strategy\Interfaces;

interface MessageInterface
{
    public function getMessage(): string;

}