<?php

declare(strict_types=1);

namespace App\Strategy;

use App\Strategy\Interfaces\MessageInterface;

readonly class MessageContext
{
    public function __construct(
        protected MessageInterface $dayOfWeek
    ){}

    public function getMsgDayOfWeek(): string
    {
       return $this->dayOfWeek->getMessage();
    }
}