<?php

namespace App\Strategy;

use App\Strategy\DayOfWeek\Days;
use App\Strategy\Interfaces\MessageInterface;

class Message implements MessageInterface
{
    public function __construct(
        protected Days $dayOfWeek
    ){}

    /**
     * @throws \Exception
     */
    public function getMessage(): string
    {
      return $this->dayOfWeek->getMessage();
    }
}