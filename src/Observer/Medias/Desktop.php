<?php

declare(strict_types=1);

namespace App\Observer\Medias;

use App\Observer\Interfaces\MediaInterface;

readonly class Desktop implements MediaInterface
{
    public function __construct(
        protected string $name
    ){}

    public function update(string $message): void
    {
        echo "Notificação no aplicativo desktop {$this->name}: $message </br>";
    }
}