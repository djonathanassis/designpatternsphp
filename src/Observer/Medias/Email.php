<?php

declare(strict_types=1);

namespace App\Observer\Medias;

use App\Observer\Interfaces\MediaInterface;

class Email
{
    protected array $notifications = [];

    public function attach(MediaInterface $notification): void
    {
        $this->notifications[] = $notification;
    }

    public function detach(MediaInterface $notification): void
    {
        $key = array_search($notification, $this->notifications, true);
        if ($key !== false) {
            unset($this->notifications[$key]);
        }
    }

    public function notify(string $message): void
    {
        foreach ($this->notifications as $notification) {
            $notification->update($message);
        }
    }

    public function new(string $message): void
    {
        echo "Novo e-mail recebido: $message </br>";
        $this->notify($message);
    }

}