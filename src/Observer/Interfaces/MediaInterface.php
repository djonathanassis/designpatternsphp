<?php

declare(strict_types=1);

namespace App\Observer\Interfaces;

interface MediaInterface
{
    public function update(string $message): void;
}