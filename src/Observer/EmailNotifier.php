<?php

declare(strict_types=1);

namespace App\Observer;

use App\Observer\Interfaces\MediaInterface;
use App\Observer\Medias\Email;

class EmailNotifier
{
    protected Email $email;
    public function __construct(
    ){
        $this->email = new Email();
    }

    public function attachNotify(MediaInterface $notifier): void
    {
        $this->email->attach($notifier);
    }

    public function detachNotify(MediaInterface $notifier): void
    {
        $this->email->detach($notifier);
    }

    public function newNotify(string $notifier): void
    {
        $this->email->new($notifier);

    }
}